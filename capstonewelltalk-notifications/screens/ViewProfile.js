import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { TextInput } from 'react-native-web';
import axios from 'axios';

export default function ViewProfile({ navigation, route }) {
  const { idNo } = route.params;
  const [first_Name, setFirstName] = useState('');
  const [last_Name, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [course, setCourse] = useState('');
  const [contact_No, setContactNo] = useState('');
  const [id_No, setIdNo] = useState('');
  const [password, setPassword] = useState('');
  
  useEffect(() => {
    fetchUserEntry();
  }, []);

  const fetchUserEntry = () => {
    axios
      .get(`http://localhost:8080/userGet/${idNo}`)
      .then(response => {
        const { first_Name, last_Name, email, course, contact_No, id_No, password } = response.data;
        setFirstName(first_Name);
        setLastName(last_Name);
        setEmail(email);
        setCourse(course);
        setContactNo(contact_No);
        setIdNo(id_No);
        setPassword(password);
      })
      .catch(error => {
        console.log('Error retrieving user:', error);
      });
  };
  return (
    <View style={styles.container}>
      <View>
        <img
        style={styles.tinyLogo}
        src={require('./Photos/usericon.png')}
        ></img>
      </View>
      <Text style={styles.label1}>Name</Text>
      <TextInput
        placeholder="Name"
        disabled
        style={styles.input}
        value={first_Name + " " + last_Name}
      />
      <Text style={styles.label2}>Email</Text>
      <TextInput
        placeholder="Email"
        disabled
        style={styles.input}
        value={email}
      />
      <Text style={styles.label3}>Program/Course</Text>
      <TextInput
        placeholder="Program/Course"
        disabled
        style={styles.input}
        value={course}
      />
      <Text style={styles.label4}>Contact Number</Text>
      <TextInput
        placeholder="Contact Number"
        disabled
        style={styles.input}
        value={contact_No}
      />
      <Text style={styles.label5}>ID Number</Text>
      <TextInput
        placeholder="ID Number"
        disabled
        style={styles.input}
        value={id_No}
      />
      <Text style={styles.label6}>Password</Text>
      <TextInput
        placeholder="Password"
        disabled
        value={password}
        secureTextEntry={true}
        style={styles.input}
      />
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 20,
      color: '#ffffff',
      alignItems: 'center',
      backgroundColor: '#ffffff',
    },
    input: {
      width: 300,
      height: 44,
      padding: 10,
      marginTop: 10,
      marginBottom: 10,
      backgroundColor: '#ffffff',
      borderColor: '#30D5C8',
      borderWidth: 1,
      borderRadius: 10,
      activeUnderlineColor: 'red',
    },
    tinyLogo: {
      backgroundColor: '#ffffff',
      position: 'relative',
      top: 0,
      width: 100,
      height: 100,
      borderRadius: 100 / 2,
    },
    label1: {
      position: 'relative',
      right: 130,
    },
    label2: {
      position: 'relative',
      right: 130,
    },
    label3: {
      position: 'relative',
      right: 98,
    },
    label4: {
      position: 'relative',
      right: 98,
    },
    label5: {
      position: 'relative',
      right: 115,
    },
    label6: {
      position: 'relative',
      right: 120,
    },
  });

