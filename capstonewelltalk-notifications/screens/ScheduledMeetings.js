import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

const ScheduledMeetings = ({ navigation, route }) => {
  const { idNo } = route.params;

  return (
    <View style={styles.container}>
      <Text>Some text</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ScheduledMeetings;