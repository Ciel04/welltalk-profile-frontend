import ViewProfile from "./screens/ViewProfile";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Test from "./screens/Test";
import Notifications from "./screens/Notifications";
import ScheduledMeetings from "./screens/ScheduledMeetings";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
     <Stack.Navigator>
        <Stack.Screen
    
        name="Test"
        component={Test}
        options={{ title: "Test", headerTitleStyle: {
      color: "#30d5c8" // Set the desired font color here
    } }}
      />
      <Stack.Screen
    
        name="Notifications"
        component={Notifications}
        options={{ title: "Notifications", headerTitleAlign: 'center', headerTitleStyle: {
      color: "#ffffff" // Set the desired font color here
    }, headerTintColor: '#ffffff',
    headerStyle:{ backgroundColor: '#30d5c8'}}}
    />
    <Stack.Screen
    
        name="ScheduledMeetings"
        component={ScheduledMeetings}
        options={{ title: "ScheduledMeetings", headerTitleAlign: 'center', headerTitleStyle: {
      color: "#ffffff" // Set the desired font color here
    }, headerStyle:{ backgroundColor: '#30d5c8'}}}
    />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
